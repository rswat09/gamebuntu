#!/bin/bash

if [ "$(dpkg-query -W --showformat='${db:Status-Status}' gamehub-git 2>&1)" = installed ]; then
  echo 'GameHub is already installed.'
  sleep 3 && exit
fi

sudo apt-get install -y software-properties-common
sudo add-apt-repository -y universe
sudo add-apt-repository -y multiverse
una install gamehub-git